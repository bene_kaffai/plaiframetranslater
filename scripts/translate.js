// (function (window, document, undefined) {

////////////////////////////////////////////////////////////////////////////
// SELECTORS
////////////////////////////////////////////////////////////////////////////
const table = document.getElementById('cue-table');
const save_copy = document.getElementById('save-copy');
const load_original = document.getElementById('load-original');
const load_copy = document.getElementById('load-copy');
const copy_original = document.getElementById('copy-original');
const downloader = document.getElementById('downloader');

////////////////////////////////////////////////////////////////////////////
// GLOBAL VARIABLES 
////////////////////////////////////////////////////////////////////////////
const ORIGINAL = {}, COPY = {};
const ALLOWED_KEYS = ['text', 'respond', 'contains', 'regex', 'equals', 'value', 'play', 'say', 'flow']
/*
TODO: use paths not keys to validate if value is content 
const ALLOWED_PATHS = [['smsout', 'text'],['smsin', 'if', 'contains'], ...];
*/

////////////////////////////////////////////////////////////////////////////
// METHODES 
////////////////////////////////////////////////////////////////////////////

// go through cues and there actions in level and create table
function populateTable(tbl, keywords, cueListA, cueListB) {
    // TODO: really check if both level have the same keys
    if (ORIGINAL.name !== COPY.name) {
        alert('Loaded Levels are different. Unable to display side by side.');
        return false;
    }

    // go through every cue and action
    for (const index in cueListA) {
        // add cue name as title
        addCell('title', tbl.insertRow(), cueListA[index], 'name');
        for (const action in cueListA[index]) {
            // look for allowed keys in every action
            traverseObj(tbl, keywords,
                cueListA[index][action], cueListB[index][action], [action]);
        }
    }
}

/*
* walks through given object and looks for keywords. 
* assumes obj1 and obj2 have same structure
* 
* @param {element} table -reference to table 
* @param {array} keywords - list of all allowed keys (should be path)
* @param {object} obj1 -  
* @param {object} obj2 -  
* @param {array} path - path to current key. gets longer on recursions
*/
function traverseObj(tbl, keywords, obj1, obj2, path = []) {

    for (const [key, value] of Object.entries(obj1)) {
        path.push(key); // add current key to path

        /*
        * TODO: should check the whole path against allowd paths. bspw.:
        * allowed_paths.forEach(allowed => { 
        *    if ((path, allowed) => allowed.every(v => path.includes(v))) {} 
        * });
        */
        if (keywords.includes(key)) {
            // because 'respond' is a array, not a string
            if (Array.isArray(obj1[key])) {
                for (const index in obj1[key]) {
                    let row = tbl.insertRow();
                    addCell('info', row, [path.join('.')])
                    addCell('show', row, obj1[key], index);
                    addCell('edit', row, obj2[key], index,
                        { counter: (['text', 'respond'].includes(key)) });
                }
            } else {
                let row = tbl.insertRow();
                addCell('info', row, [path.join('.')])
                addCell('show', row, obj1, key);
                addCell('edit', row, obj2, key,
                    { counter: (['text', 'respond'].includes(key)) });
            }
        }
        if (typeof value === 'object') {
            // go deeper in given objects
            traverseObj(tbl, keywords, obj1[key], obj2[key], path);
        }
        path.pop(); // remove key from current path
    }

    return true
}

/*
* create new cell in table based on type and content
* 
* @param {string} type - what type of cell: show, info, edit, title
* @param {object} object - object to reference to 
* @param {string} key - what value should be dispayed out of the object 
* @param {element} row - row element to place cell in 
* @param {object} options - define additional options (currently not in use) 
*
* type of cell: 
* show:cell to display text, info:same as show; no border 
* edit: creates editable div to change value in reference, title: spans row
*/
function addCell(type = 'show', row, obj, key, options = {}) {
    // create config for cells in table
    const defaults = { col: -1, counter: false };
    const config = Object.assign({}, defaults, options);

    // if only object but no key was given use first value
    key = key ? key : 0;

    // create cell for content and span for charcater count
    let cell = row.insertCell(config.col);
    let charcount = document.createElement('span');

    // add classes based on type selected
    cell.classList.add(type);
    charcount.classList.add('counter');

    // switch different cell types to create different layouts
    switch (type) {
        case 'edit':
            const edit = document.createElement('textarea');
            edit.value = obj[key];
            cell.appendChild(edit);
            edit.style.height = "1px";
            edit.style.height = `${15 + edit.scrollHeight}px`;
            cell.oninput = (e) => {
                // obj[key] = e.srcElement.textContent;
                const node = e.srcElement;
                obj[key] = node.value;
                node.style.height = "1px";
                node.style.height = `${15 + node.scrollHeight}px`;
                charcount.textContent = `${obj[key].length} | ${Math.ceil(obj[key].length / 160)} sms`;
            }
            break;
        case 'title':
            cell.appendChild(document.createTextNode(obj[key]));
            cell.setAttribute('colSpan', 3);
            break;
        default:
            cell.innerText = obj[key];
            break;
    }

    // add little textbox with character length
    if (config.counter) {
        charcount.textContent = `${obj[key].length} | ${Math.ceil(obj[key].length / 160)} sms`;
        cell.appendChild(charcount);
    }
}

////////////////////////////////////////////////////////////////////////////
// CALLBACK METHODS TO SAVE AND LOAD LEVEL DATA
////////////////////////////////////////////////////////////////////////////

// open download dialoge and saves json files
function saveFile(data, fileName, addTimestamp = true) {
    if (addTimestamp) {
        const time = new Date();
        fileName += `_${time.getDate()}${time.getMonth() + 1}_${time.getHours()}_${time.getMinutes()}.json`
    }
    let json = JSON.stringify(data, null, 2),
        blob = new Blob([json], { type: 'octet/stream' }),
        url = window.URL.createObjectURL(blob);
    downloader.href = url;
    downloader.download = fileName;
    downloader.click();
    window.URL.revokeObjectURL(url);
}

// called when uploader input field is clicked
function readFile(evt, ref) {
    let file = evt.target.files[0];
    if (!file) return;
    let reader = new FileReader();
    reader.onload = function (evt) {
        Object.assign(ref, JSON.parse(evt.target.result));
        document.title = `Translater: ${ref.name}`;
        if (Object.keys(ORIGINAL).length && Object.keys(COPY).length) {
            populateTable(table, ALLOWED_KEYS,
                ORIGINAL.chapter.chapter,
                COPY.chapter.chapter);
        };
    };
    reader.readAsText(file);
}

////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////

// Press 'F2'/STR+s/CTL+s to save/download
document.addEventListener('keydown', (evt) => {
    if (COPY && (evt.keyCode === 113) ||
        (evt.ctrlKey && evt.keyCode == 83) ||
        (evt.keyCode == 91 && evt.keyCode == 83)) {
        evt.preventDefault();
        saveFile(COPY, `${COPY.name}_COPY`);
    }
}, false);

// Download Level data on button click
save_copy.addEventListener('click', (evt) => {
    saveFile(COPY, `${COPY.name}_COPY`);
}, false);

// Load new Level data on dutton click
load_original.addEventListener('change', (evt) => {
    readFile(evt, ORIGINAL);
}, false);
load_copy.addEventListener('change', (evt) => {
    readFile(evt, COPY);
}, false);

// copy ORIGINAL level data to COPY
copy_original.addEventListener('click', (evt) => {
    if (Object.keys(ORIGINAL).length) {
        Object.assign(COPY, JSON.parse(JSON.stringify(ORIGINAL)));
        populateTable(table, ALLOWED_KEYS,
            ORIGINAL.chapter.chapter,
            COPY.chapter.chapter);
    }
}, false);


// })(window, document);