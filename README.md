# �bersetzungstool f�r Patrol

Das Tool kann _LEVEL.json_ Dateien einlesen. In einer Tabelle wird links das Original Level angezeigt, rechts ein bearbeitbares Level. Das original Level kann kopiert werden, oder ein zweites Level zum Bearbeiten geladen werden.
Folgende Keys werden angezeigt.

- text
- contains
- equals
- regex
- response
- flow
- say
- play
- value

## Benutzung

Die Tastenkombinationen 'STRG + s', 'CTL + s', 'F2' speichern die bearbeitete Version des Levels.

## TODO

1. Es sollte unbedingt nicht nach Keys, sondern nach erlaubten Pfaden gesucht werden: Nicht: *text*, sondern *smsout > [] > text* oder *smsin > [] > if    > [] > contains && response > []*
2. Es m�ssen die beiden Level Dateien auf gleiche Struktur �berpr�ft werden. 
